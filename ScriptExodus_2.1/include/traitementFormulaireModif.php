<?php

	/*--------------------------------------------------------------------------------------*\
	
		Quand l'utilisateur vient du mode 'Modification', il faut extraire son archive, 
	et surtout récupérer ses données dans le fichier 'dataExodus'. Cela permet de créer le
	bloc HTML de la table des matières, d'écrire un fichier marqueur si l'utilisateur n'en a
	pas fournit pour permettre à la page 'ecritureTimeSheetSmil.php de lire un fichier 
	marqueur. Puis de remplir le tableau 'stockInfoFichierServeur' tout le long du programme
	
	\*--------------------------------------------------------------------------------------*/

	//Si on a reçu le fichier, donc que l'attribut 'upload' est à TRUE, on extrait le Zip dans le dossier de l'Exodus
	if ($stockInfoFichierServeur['fichierZip']['upload'] == TRUE){

		//Récupère le chemin du zip
		$cheminZip = $stockInfoFichierServeur['fichierZip']['chemin'];

		//Créer une instance ZipArchive - S'il n'y a pas d'erreur lors de son ouverture, extrait l'archive, ferme l'archive active et la supprime
		$zip = new ZipArchive;  
		if($zip->open($cheminZip)) {		
			  $zip->extractTo('dossierUt/'.$nomDossierUt.'/Masque_Exodus');  
			  $zip->close();  
		}  
		unlink($cheminZip); 
	
	}
		
	//Si l'Utilisateur n'upload pas son, il faut pointer les informations sur le son présent dans son Zip pour pouvoir écrire le fichier 'audio.html' sans soucis
	if ($stockInfoFichierServeur['fichierAudio']['upload'] == FALSE){
		//Grâce à cette fonction, on récupère les informations du son du Zip et on stock ces informations dans le tableau 'stockInfoFichierServeur'
		$fichierDefaut = trouverFichier('fichierAudio');
		$stockInfoFichierServeur['fichierAudio'] = array('chemin'=> $fichierDefaut['chemin'], 'extension'=> $fichierDefaut['ext'], 'nom'=> $fichierDefaut['nom'], 'upload'=> FALSE);
	}
	//Si l'Utilisateur n'upload pas d'image, il faut pointer les informations sur l'image présent dans son Zip pour pouvoir écrire le fichier 'audio.html' sans soucis
	if ($stockInfoFichierServeur['fichierImage']['upload'] == FALSE){
		//Grâce à cette fonction, on récupère les informations de l'image du Zip et on stock ces informations dans le tableau 'stockInfoFichierServeur'
		$fichierDefaut = trouverFichier('fichierImage');
		$stockInfoFichierServeur['fichierImage'] = array('chemin'=> $fichierDefaut['chemin'], 'extension'=> $fichierDefaut['ext'], 'nom'=> $fichierDefaut['nom'], 'upload'=> FALSE);
	}
	
	//Si on a reçu le fichier, donc que l'attribut 'upload' est à TRUE, on ouvre le fichier 'data' de l'Utilisateur
	//Sinon on ouvre le fichier 'dataExodus' du Zip
	if ($stockInfoFichierServeur['fichierData']['upload'] == TRUE){
		$fichierDataExodus = fopen("dossierUt/".$nomDossierUt."/dataExodus.txt", "r");
	} else {
		$fichierDataExodus = fopen("dossierUt/".$nomDossierUt."/".$nomExodus."/edit/dataExodus.txt", "r");
	}

	//Lit la ligne suivante du fichier et scinde la chaîne de caractères en segments dans un tableau, ce qui donne : [1]=>'titreConference', [2]=>'leTitre'
	//Le séparateur étant "|" (titreConference|leTitre)
	$ligneActuelle = explode('|',fgets($fichierDataExodus));
	//Tant qu'on a pas finit la 1er Partie du fichier data, donc qu'on a pas atteint le saut de ligne et le '|' ce qui donnerait : [1]=>'' ; on fait
	while($ligneActuelle[0] != ""){
		//Étant donné que le nom des données est sur la première colonne, et que la valeur des données est sur la seconde,
		//pas besoin de déclarer chacune des variables à la main, 'explode' nous permet de construire le nom de la variable
		//avec le premier élément du tableau (tout en déclarant cette dernière) et de lui attribuer sa valeur avec le second élément du tableau
		${$ligneActuelle[0]} = $ligneActuelle[1];
		$ligneActuelle = explode('|',fgets($fichierDataExodus));
	}

	//Initialisation de la chaîne 'blocTDM' qui est construite différemment que celle construite par le formulaire - 
	//Initialisation de la chaîne 'blocTDMData' qui est le bloc de la TDM qui va aller dans le fichier data, elle est construite différemment aussi
	//puisque ici on construit nos bloc HTML et le fichier data, à partir du fichier data de l'utilisateur et non un formulaire
	$blocTDM = ""; // TDM => Table Des Matières
	$blocTDMData = "";
	$ligneActuelle = explode('|',fgets($fichierDataExodus));
	//Tant qu'on a pas lu tout le bloc TDM du fichier 'dataExodus.txt', on ajoute une ligne à la liste de la TDM
	//soit il n'y a pas de sous partie et on ne rentre pas dans le if et on ferme la ligne directement
	//soit il y a une sous partie et on ajoute autant de fois qu'il le faut de sous partie
	//On écrit en même temps le bloc de la TDM pour le fichier data
	while($ligneActuelle[0] != ""){
		//Écriture du bloc de la TDM pour le fichier data
		$blocTDMData .= "Partie|".$ligneActuelle[1]."|".$ligneActuelle[2].'|'.PHP_EOL;
		//On a besoin de remplacer plusieurs mots clés, on utilise des tableaux pour faire cela
		//on va rechercher les mots clés 'diapo' et 'Titre'
		$recherche  = array('diapo', 'Titre');
		//Pour les remplacer par la diapo sélectionné de la partieX et le nom de la partie
		$remplace = array($ligneActuelle[2], $ligneActuelle[1]);
		//Dans la ligne html ci dessous
		$cible = "						<li><a href=\"#diapo\">Titre</a>";
		//Puis on applique la fonction avec les différents tableaux et on passe à la ligne suivante
		$blocTDM .= str_replace($recherche, $remplace, $cible);
		$ligneActuelle = explode('|',fgets($fichierDataExodus));
		//Si la ligne suivante est une sous partie alors on fait
		if ($ligneActuelle[0] == 'SousPartie'){	
			//Ajout d'une liste à la partieX
			$blocTDM .= "\n							<ul>\n";
			//Tant qu'il y a une sousPartieX_Y (tant que la ligne suivante est une sous partie), on ajoute une ligne à liste (donc on ajoute une sous partie à la partieX)
			for($posSousPartie = 1; $ligneActuelle[0] == 'SousPartie'; $posSousPartie++){
				///Écriture du bloc de la TDM pour le fichier data
				$blocTDMData .= "SousPartie|".$ligneActuelle[1]."|".$ligneActuelle[2].'|'.PHP_EOL;
				//Idem que plus haut, on a plusieurs mots clés à changer dans la ligne html, on utilise le même principe grâce aux tableaux
				$recherche  = array('diapo', 'Titre');
				$remplace = array($ligneActuelle[2], $ligneActuelle[1]);
				$cible = "								<li><a href=\"#diapo\">Titre</a></li>\n";
				$blocTDM .= str_replace($recherche, $remplace, $cible);
				$ligneActuelle = explode('|',fgets($fichierDataExodus));
			}
			//Une fois qu'il n'y a plus de sousPartieX_Y, on ferme la liste de la partieX
			$blocTDM .= "							</ul>\n						";
		}
		//Une fois qu'on a finit d'écrire la ligne, on la ferme
		$blocTDM .= "</li>\n";
	}

	//Si l'Utilisateur n'a pas upload de fichier 'marqueur', alors on le construit et on stock ses informations dans le tableau 'stockInfoFichierServeur'
	if($stockInfoFichierServeur['fichierMarqueur']['upload'] == FALSE){
		$file = fopen("dossierUt/".$nomDossierUt."/marqueur.txt", "a+");
		$ligneActuelle = explode('|',fgets($fichierDataExodus));
		while($ligneActuelle[0] != ""){
			fwrite($file,$ligneActuelle[1]."	".PHP_EOL);
			$ligneActuelle = explode('|',fgets($fichierDataExodus));
		}
		fclose($file);
		$stockInfoFichierServeur['fichierMarqueur'] = array('chemin'=> 'dossierUt/'.$nomDossierUt.'/marqueur.txt', 'extension'=> 'txt', 'nom'=>'marqueur', 'upload'=> TRUE);
	}
	
	fclose($fichierDataExodus);
		
?>