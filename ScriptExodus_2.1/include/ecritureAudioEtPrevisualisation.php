<?php

	/*--------------------------------------------------------------------------------------*\
	
			Construit la page 'audio.html' ainsi que la page 'previsualisation.html'
	
	\*--------------------------------------------------------------------------------------*/

	
	////-------------------------------- Bloc images --------------------------------\\\\
	
	//Initialisation de la chaîne 'blocImages'
	$blocImages = "";
	//Tant qu'on arrive pas au nombre de diapo, on ajoute à la chaîne une ligne html pour la div des images
	for($i = 1; $i <= $nombreDeDiapo; $i++){
		//Formate la chaîne pour avoir 'diapoXXX' sur 3 digits - on met en argument la chaîne à formater avec dedans '%03s' qui veut dire 3 digits et on le considère comme un string + 'i' qui est la cible à formater sur 3 digits
		$nomDiapo = sprintf("diapo%03s", $i);
		//On remplace le mot clé 'numDiapo' par la variable 'nomDiapo' (diapoXXX) dans la ligne html ci dessous
		$blocImages .= str_replace("numDiapo", $nomDiapo , "				<img id='numDiapo' alt='numDiapo' src='diapos/numDiapo.jpg'>\n");
	}
	//Initialisation de la chaîne 'blocImagesEdition' pour le mode Correction
	$blocImagesEdition = "";
	//Tant qu'on arrive pas au nombre de diapo, on ajoute à la chaîne une ligne html pour la div des images
	for($i = 1; $i <= $nombreDeDiapo; $i++){
		//Formate la chaîne pour avoir 'diapoXXX' sur 3 digits - on met en argument la chaîne à formater avec dedans '%03s' qui veut dire 3 digits et on le considère comme un string + 'i' qui est la cible à formater sur 3 digits
		$nomDiapo = sprintf("diapo%03s", $i);
		//On remplace le mot clé 'numDiapo' par la variable 'nomDiapo' (diapoXXX) dans la ligne html ci dessous
		$blocImagesEdition .= str_replace("numDiapo", $nomDiapo , "				<img id='numDiapo' alt='numDiapo' src='../diapos/numDiapo.jpg'>\n");
	}
	
	////-------------------------------- Bloc TDM --------------------------------\\\\
	
	//Si l'utilisateur n'est pas en mode Modification, il faut créer le 'blocTDM'
	if (!(isset($_POST['modifData']))){
		//Initialisation de la chaîne 'blocTDM'
		$blocTDM = ""; // TDM => Table Des Matières
		//Tant qu'on arrive pas au nombre de partie, on ajoute une ligne à la liste de la TDM
		//soit il n'y a pas de sous partie et on ne rentre pas dans le if et on ferme la ligne directement
		//soit il y a une sous partie et on ajoute autant de fois qu'il le faut de sous partie
		for($posPartie = 1; $posPartie <= $nombreDePartie; $posPartie++){ //posPartie => positionPartie
			
			//On a besoin de remplacer plusieurs mots clés, on utilise des tableaux pour faire cela
			//On va rechercher les mots clés 'diapo' et 'Titre'
			$recherche  = array('diapo', 'Titre');
			//Pour les remplacer par la diapo sélectioné de la partieX et le nom de la partie
			$remplace = array($_POST['diapoPartie'.$posPartie], $_POST['nomPartie'.$posPartie]);
			//Dans la ligne html ci dessous
			$cible = "						<li><a href=\"#diapo\">Titre</a>";
			//Puis on applique la fonction avec les différents tableaux
			$blocTDM .= str_replace($recherche, $remplace, $cible);
			
			//Si il y a au moins une sous partie à la partieX, alors 'nomSousPartieX_1' est déclaré, et donc si c'est bien le cas on fait
			if (isset($_POST['nomSousPartie'.$posPartie.'_1'])){	
				//Ajout d'une liste à la partieX
				$blocTDM .= "\n							<ul>\n";
				//Tant qu'il y a une sousPartieX_Y, on ajoute une ligne à liste (donc on ajoute une sous partie à la partieX)
				for($posSousPartie = 1; isset($_POST['nomSousPartie'.$posPartie.'_'.$posSousPartie]); $posSousPartie++){
					//Idem que plus haut, on a plusieurs mots clés à changer dans la ligne html, on utilise le même principe grâce aux tableaux
					$recherche  = array('diapo', 'Titre');
					$remplace = array($_POST['diapoSousPartie'.$posPartie.'_'.$posSousPartie], $_POST['nomSousPartie'.$posPartie.'_'.$posSousPartie]);
					$cible = "								<li><a href=\"#diapo\">Titre</a></li>\n";
					$blocTDM .= str_replace($recherche, $remplace, $cible);
				}
				//Une fois qu'il n'y a plus de sousPartieX_Y, on ferme la liste de la partieX
				$blocTDM .= "							</ul>\n						";
			}
			//Une fois qu'on a finit d'écrire la ligne, on la ferme
			$blocTDM .= "</li>\n";
		}
	}
	
	////-------------------------------- Pied de page --------------------------------\\\\
	
	//Affichage 'Prise de son et réalisation' en fonction de si le champ a été renseigné ou pas, si un des deux a été renseigné ou si aucun n'a été renseigné
	if($priseSon == $realisation && $priseSon !=""){
		$priseSonReal = "Prise de son et réalisation : ".$priseSon." ";
	} else if ($priseSon != "" && $realisation == "") {
		$priseSonReal = "Prise de son : ".$priseSon." ";
	} else if ($priseSon == "" && $realisation != ""){
		$priseSonReal = "Réalisation : ".$realisation." ";
	} else if ($priseSon != "" && $realisation != "") {
		$priseSonReal = "Prise de son : ".$priseSon." - Réalisation : ".$realisation." ";	
	} else {
		$priseSonReal = "";
	}
	
	//Si il y a au moins une personne pour la prise son et/ou la réalisation, ajoute un retour à la ligne pour l'indentation du code
	if($priseSonReal !=""){
		$priseSonReal .= "\n				";
	}
	
	//Récupère sous forme de tableau les informations d'une date donnée (année, mois, jour, heure, minute...) (ex: [day]=> 8; [hour]=>0) 
	//Nous avons besoin du jour et de l'année, et pour ce qui est du mois, grâce à un switch nous allons le convertir en lettre
	$tableauDateInfo = date_parse($dateConference);
	$jour = $tableauDateInfo['day'];
	$annee = $tableauDateInfo['year'];
	switch ($tableauDateInfo['month']) {
		case 1:
			$mois = 'Janver';
			break;
		case 2:
			$mois = 'Février';
			break;
		case 3:
			$mois = 'Mars';
			break;
		case 4:
			$mois = 'Avril';
			break;
		case 5:
			$mois = 'Mai';
			break;
		case 6:
			$mois = 'Juin';
			break;
		case 7:
			$mois = 'Juillet';
			break;
		case 8:
			$mois = 'Août';
			break;
		case 9:
			$mois = 'Septembre';
			break;
		case 10:
			$mois = 'Octobre';
			break;
		case 11:
			$mois = 'Novembre';
			break;
		case 12:
			$mois = 'Décembre';
			break;
	}
	//puis on concaténé toutes ses informations dans une variable
	$chaineDate = $jour." ".$mois." ".$annee;


	//Écrit le fichier 'audio.html' et 'previsualisation.html' en remplissant les points clés par les informations remplies dans le formulaire et 
	//par les bouts d'HTML construit plus haut

	//Stock l'extension et le nom complet du fichier audio dans 2 variables - ext => extension
	$extAudio = $stockInfoFichierServeur['fichierAudio']['extension'];
	$nomAudio = $stockInfoFichierServeur['fichierAudio']['nom'].".".$stockInfoFichierServeur['fichierAudio']['extension'];
	
	//Stock le nom complet du fichier image dans une variable
	$nomImage = $stockInfoFichierServeur['fichierImage']['nom'].".".$stockInfoFichierServeur['fichierImage']['extension'];
	

	////-------------------------------- Texte 'audio.html' --------------------------------\\\\


	$texteAudioHtml = "<!DOCTYPE html>
<html lang='fr'>

	<head>
		<meta charset='utf-8'>
		<title>".$titreConference."</title>  <!-- ici, le texte qui apparaîtra dans la barre de titre de la fenêtre du navigateur -->

		<link rel='timesheet' type='application/smil+xml' href='smil/timesheet.smil'>   <!--appel du fichier de synchronisation qui gère tout l'ensemble-->

		<!-- ci-dessous, on choisit la position de la table des matières par rapport aux diapositives, droite, ou gauche). Commenter l'un ou l'autre selon besoin    
		<!--  <link rel='stylesheet' type='text/css' href='tools/style/audio-right.css'>   <!--il existe un audio-left.css aussi-->
		<link rel='stylesheet' type='text/css' href='tools/style/audio-left.css'>   <!--il existe un audio-right.css aussi-->
		 
		<!-- appel des librairies qui gèrent les mouvements et le son, la vidéo, etc, en HTML5. Ne rien toucher ! -->    
		<script type='text/javascript' src='tools/js/timesheets.min.js'></script>
		<script type='text/javascript' src='tools/js/timesheets-controls.min.js'></script>
		<script type='text/javascript' src='tools/js/timesheets-navigation.min.js'></script>
	</head>
	
	<body>
		<header>

			<!-- photo de l'auteur et logo de l'ENM doivent être dans le répertoire images. Appeler la photo PhotoAuteur.jpg ou changer le lien ci-dessous -->
			<img id='PhotoAuteur' alt=\"Photo de l'auteur\" src='images/".$nomImage."' height='40px'>
			<a href='https://moodle.enm.meteo.fr/'><img id='logo_enm' alt='ENM' src='images/logo.jpg'></a>
			<!-- mettre en commentaire le nav si dessous s'il n'y a pas de version vidéo associée -->
			<!--        <nav>
				  audio <a href='video.html'>vidéo</a>
				</nav> -->

			<!-- Informations concernant la conférence. On peut ajouter des lignes dans le paragraphe <p> final, sinon, le laisser vide pour des questions de hauteurs du logo ENM -->        
			<h1>".$titreConference."</h1>
			<p>".$prenomEnseignant." ".$nomEnseignant.", ".$service.", ".$employeur."</p>
			<p>&nbsp;</p> <!--pour que le logo rentre en hauteur --> 
			
		</header>

		<!-- Définition du diaporama. On a un effet de transition entre diapo appelé cross-fade. Il en existe d'autres
		Les diapositives doivent être dans le répertoire diapos, de préférence avec une racine de nom pour faciliter l'écriture ci-dessous. Changer les liens ci-dessus autant que nécessaire (par exemple pour utiliser une gif animée comme dans la deuxième diapo pour ce fichier). On peut s'aider du fichier Calc associé MasterListerDiapos -->
		<div id='slideshow' class='cross-fade'>
			<button id='swap'><span>⤢</span></button>      
			<div id='images'>
				
".$blocImages."
			</div>

			<!-- appel du fichier son. Trois formats sont possibles, on lit ce qui existe. Appeler le fichier son audioconf.mp3 et le mettre dans le répertoire sons. 
			L'option autoplay permet de démarrer dès l'affichage de la page. La commenter si on ne souhaite pas ce comportement -->
			  
			<audio id='talk' autoplay>
			  <source type='audio/".$extAudio."' src='sons/".$nomAudio."' />
			</audio>

			<!-- Définition de la table des matières. Indiquer le nom des diapos correspondant aux changements de chapitre et/ou sous-chapitres. Inutile de répertorier chaque diapo, le curseur permet de se déplacer si besoin.  On imbrique les balises ul sous les li pour faire des sous-parties.  -->
			  
			<nav id='timeController'>
				<!-- Table of Contents -->
				<div class='smil-toc'>
					<ul class='smil-tocList'>
						
".$blocTDM."
					</ul>
				</div>

				<!-- partie qui concerne la timeline. On ne touche RIEN ! --> 
				<!-- timeline + control buttons -->
				<div class='smil-controlBar'>
					<div class='smil-left'>
						<button class='smil-first'><span>|«</span></button>
						<button class='smil-prev'><span>«</span></button>
						<button class='smil-play'><span>▶||</span></button>
						<button class='smil-next'><span>»</span></button>
						<button class='smil-last'><span>»|</span></button>
					</div>
					<div class='smil-timeline'>
						<div class='smil-timeSlider'></div>
						<div class='smil-timeSegments'></div>
						<div class='smil-timeCursor'></div>
					</div>
					<div class='smil-right smil-currentTime'>0:00:00</div>
				</div>
				
				<div class='smil-tocDisplay'></div>
				
			</nav>
		</div>

		<footer>
			<div style='float:right;'>
				<!-- pied de page avec informations « légales » à adapter -->    
				<p> ".$titreConference." - ".$chaineDate."</p>
				<p> 
					".$priseSonReal."— Synchronisation : <a href='http://tyrex.inria.fr/timesheets/'>timesheets.js</a>
					— Outil proposé par <a href='http://www.enm.meteo.fr/'>INP-ENM</a>
				</p>
			</div>
		</footer>
	</body>
</html>";

	////-------------------------------- Texte 'previsualisation.html' --------------------------------\\\\

	$textePrevisualisationHtml = "<!DOCTYPE html>
<html lang='fr'>

	<head>
		<meta charset='utf-8'>
		<title>".$titreConference."</title>  <!-- ici, le texte qui apparaîtra dans la barre de titre de la fenêtre du navigateur -->

		<link rel='timesheet' type='application/smil+xml' href='../smil/timesheet.smil'>   <!--appel du fichier de synchronisation qui gère tout l'ensemble-->

		<!-- ci-dessous, on choisit la position de la table des matières par rapport aux diapositives, droite, ou gauche). Commenter l'un ou l'autre selon besoin    
		<!--  <link rel='stylesheet' type='text/css' href='../tools/style/audio-right.css'>   <!--il existe un audio-left.css aussi-->
		<link rel='stylesheet' type='text/css' href='../tools/style/audio-left.css'>   <!--il existe un audio-right.css aussi-->
		 
		<!-- appel des librairies qui gèrent les mouvements et le son, la vidéo, etc, en HTML5. Ne rien toucher ! -->    
		<script type='text/javascript' src='../tools/js/timesheets.min.js'></script>
		<script type='text/javascript' src='../tools/js/timesheets-controls.min.js'></script>
		<script type='text/javascript' src='../tools/js/timesheets-navigation.min.js'></script>
	</head>
	
	<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
	<script>
	
		$(document).ready(function() {
		 
			$('#validationFinale').click(function(){
			
				nomArchive= prompt('Quel nom voulez vous donner à votre Archive?','".$nomExodus."');
				if(nomArchive != null){
					window.location.replace('../../../../edit/zipTelechargement.php?data=".$nomDossierUt."&nom=".$nomExodus."&nomArchive=' + nomArchive);
				}
				
			});
		
		});
	
	</script>
	
	<body>
		<header>

			<!-- photo de l'auteur et logo de l'ENM doivent être dans le répertoire images. Appeler la photo PhotoAuteur.jpg ou changer le lien ci-dessous -->
			<img id='PhotoAuteur' alt=\"Photo de l'auteur\" src='../images/".$nomImage."' height='40px'>
			<a href='https://moodle.enm.meteo.fr/'><img id='logo_enm' alt='ENM' src='../images/logo.jpg'></a>
			<!-- mettre en commentaire le nav si dessous s'il n'y a pas de version vidéo associée -->
			<!--        <nav>
				  audio <a href='video.html'>vidéo</a>
				</nav> -->

			<!-- Informations concernant la conférence. On peut ajouter des lignes dans le paragraphe <p> final, sinon, le laisser vide pour des questions de hauteurs du logo ENM -->        
			<h1>".$titreConference."</h1>
			<p>".$prenomEnseignant." ".$nomEnseignant.", ".$service.", ".$employeur."</p>
			<p>&nbsp;</p> <!--pour que le logo rentre en hauteur --> 
			
		</header>

		<!-- Définition du diaporama. On a un effet de transition entre diapo appelé cross-fade. Il en existe d'autres
		Les diapositives doivent être dans le répertoire diapos, de préférence avec une racine de nom pour faciliter l'écriture ci-dessous. Changer les liens ci-dessus autant que nécessaire (par exemple pour utiliser une gif animée comme dans la deuxième diapo pour ce fichier). On peut s'aider du fichier Calc associé MasterListerDiapos -->
		<div id='slideshow' class='cross-fade'>
			<button id='swap'><span>⤢</span></button>      
			<div id='images'>
				
".$blocImagesEdition."
			</div>

			<!-- appel du fichier son. Trois formats sont possibles, on lit ce qui existe. Appeler le fichier son audioconf.mp3 et le mettre dans le répertoire sons. 
			L'option autoplay permet de démarrer dès l'affichage de la page. La commenter si on ne souhaite pas ce comportement -->
			  
			<audio id='talk' autoplay>
			  <source type='audio/".$extAudio."' src='../sons/".$nomAudio."' />
			</audio>

			<!-- Définition de la table des matières. Indiquer le nom des diapos correspondant aux changements de chapitre et/ou sous-chapitres. Inutile de répertorier chaque diapo, le curseur permet de se déplacer si besoin.  On imbrique les balises ul sous les li pour faire des sous-parties.  -->
			  
			<nav id='timeController'>
				<!-- Table of Contents -->
				<div class='smil-toc'>
					<ul class='smil-tocList'>
						
".$blocTDM."
					</ul>
				</div>

				<!-- partie qui concerne la timeline. On ne touche RIEN ! --> 
				<!-- timeline + control buttons -->
				<div class='smil-controlBar'>
					<div class='smil-left'>
						<button class='smil-first'><span>|«</span></button>
						<button class='smil-prev'><span>«</span></button>
						<button class='smil-play'><span>▶||</span></button>
						<button class='smil-next'><span>»</span></button>
						<button class='smil-last'><span>»|</span></button>
					</div>
					<div class='smil-timeline'>
						<div class='smil-timeSlider'></div>
						<div class='smil-timeSegments'></div>
						<div class='smil-timeCursor'></div>
					</div>
					<div class='smil-right smil-currentTime'>0:00:00</div>
				</div>
				
				<div class='smil-tocDisplay'></div>
				
			</nav>
		</div>

		<footer>
			<div style='float:left;display:flex;' >
				<p class='bouton'> <a href='../../../../edit/correction.php?data=".$nomDossierUt."&amp;nom=".$nomExodus."' > Edition </a> </p>
				<p class='bouton' id='validationFinale'> <a> Validation finale </a> </p>
			</div>
			<div style='float:right;'>
				<!-- pied de page avec informations « légales » à adapter -->    
				<p> ".$titreConference." - ".$chaineDate."</p>
				<p> 
					".$priseSonReal."— Synchronisation : <a href='http://tyrex.inria.fr/timesheets/'>timesheets.js</a>
					— Outil proposé par <a href='http://www.enm.meteo.fr/'>INP-ENM</a>
				</p>
			</div>
		</footer>
	</body>
</html>";

	////-------------------------------- Ecriture des 2 fichiers --------------------------------\\\\

	$file = fopen('dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/audio.html', "w");
	fwrite($file,$texteAudioHtml);
	fclose($file);
	
	
	$file = fopen('dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/edit/previsualisation.html', "w");
	fwrite($file,$textePrevisualisationHtml);
	fclose($file);

?>