<?php

	/*--------------------------------------------------------------------------------------*\
	
			Convertit le PDF de l’utilisateur en images avec le format 'diapoXXX'
	
	\*--------------------------------------------------------------------------------------*/


	//Scanne le dossier 'diapos' de l'Exodus utilisateur et supprime tous les fichiers à l'intérieur pour avoir un dossier propre et vide
	$dossierDiapo = scandir('dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/diapos/');
	for($i = 0; $i != sizeof($dossierDiapo); $i ++){
		if ($dossierDiapo[$i] != '.' && $dossierDiapo[$i] != '..'){ 	
			unlink('dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/diapos/'.$dossierDiapo[$i]);
		}
	}


									//------- Version Windows -------\\
	/*
	//Permet de récupérer la racine sous laquelle le script courant est exécuté
	$dest = $_SERVER['DOCUMENT_ROOT'].'/projet/ScriptExodus_2.1/';	
	//Crée une nouvelle class 'imagick' qui a besoin d'avoir une chemin en absolue - Récupère le nombre de page du PDF
	$imagick = new Imagick($dest.$stockInfoFichierServeur['fichierPDF']['chemin']);
	$nbrPage = $imagick->getNumberImages();
	//Tant qu'on a pas atteint la dernière page, on convertit la page X du PDF en image 'jpg'
	for ($i = 0; $i < $nbrPage; $i++){
		//Formate la chaîne pour avoir 'diapoXXX' sur 3 digits
		$nomDiapo = sprintf("diapo%03s", $i+1);
		//Lit la page X du PDF - Écrit cette page X en 'jpg'
		$imagick->readImage($dest.$stockInfoFichierServeur['fichierPDF']['chemin'].'['.$i.']');
		$imagick->writeImage($dest.'dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/diapos/'.$nomDiapo.'.jpg'); 
	}
	*/
									//------- Version Linux -------\\
	
	//- Density permet d'avoir des images d'une qualité correcte 
	//- Scene 1 permet de commencer la numérotation à un dans le formatage 
	//- Formatage qui se fait avec un format sous la forme %03d ('diapoXXX)
	exec('convert -density 150 -scene 1 '.$stockInfoFichierServeur['fichierPDF']['chemin'].' dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/diapos/diapo%03d.jpg');
	
	
	
	//Et on supprime le PDF
	unlink('dossierUt/'.$nomDossierUt.'/'.$stockInfoFichierServeur['fichierPDF']['nom'].'.'.$stockInfoFichierServeur['fichierPDF']['extension']);
	
?>