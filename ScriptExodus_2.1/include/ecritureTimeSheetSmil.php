<?php

	/*--------------------------------------------------------------------------------------*\
	
								Construit le fichier 'timesheet.smil'
	
	\*--------------------------------------------------------------------------------------*/
	

	//Compte le nombre de ligne, donc le nombre de marqueur - Ouvre le fichier en lecture - Initialise les chaînes 'blocMarqueurs' et 'blocMarquesData'
	$nombreMarqueurs = count(file($stockInfoFichierServeur['fichierMarqueur']['chemin']));
	$fichierMarqueurs = fopen($stockInfoFichierServeur['fichierMarqueur']['chemin'], "r");
	$blocMarqueurs = "";
	$blocMarqueursData = "";
	//Tant qu'on a pas lu toutes les lignes on écrit une ligne HTML
	for($i = 1; $i <= $nombreMarqueurs; $i++){
		//Formate la chaîne pour avoir 'diapoXXX' sur 3 digits - On met en argument la chaîne à formater avec dedans '%03s' qui veut dire 3 digits et on le considère comme un string + 'i' qui est la cible à formater sur 3 digits
		$nomDiapo = sprintf("diapo%03s", $i);
		//Retourne tous les caractères avant la 1er tabulation (/!\pas espace mais bien tab/!\) grâce à l'argument true, si false : retourne tous les caractères après la 1er tabulation 
		$marqueurBrut = stristr(fgets($fichierMarqueurs),'	', true);
		//Stock le marqueur brut pour le rajouter au fichier 'dataExodus.txt' plus tard
		$blocMarqueursData .= "marqueur|".$marqueurBrut."|".PHP_EOL;
		//Conversion du marqueur qui est en seconde, en format HH:MM:SS
		$marqueurConvertit = gmdate("H:i:s", $marqueurBrut);
		//On a besoin de remplacer plusieurs mots clés, on utilise des tableaux pour faire cela
		//On va rechercher les mots clés 'marqueur' et 'diapo'
		$recherche  = array('marqueur', 'diapo');
		//Pour les remplacer par le marqueur et le nom de la diapo au bon format
		$remplace = array($marqueurConvertit, $nomDiapo);
		//Dans la ligne HTML ci dessous
		$cible = "		<item select=\"#diapo\" begin=\"marqueur\"/>\n";
		//Puis on applique la fonction avec les différents tableaux
		$blocMarqueurs .= str_replace($recherche, $remplace, $cible);
	}
	fclose($fichierMarqueurs);

	
	//On écrit tout le fichier timesheet.smil en remplissant l'attribut 'dur' par la durée de la présentation demandé dans le formulaire 
	//et on insère le bloc HTML des marqueurs au bon endroit
	$file = fopen("dossierUt/".$nomDossierUt."/".$nomExodus."/smil/timesheet.smil", "w");
	fwrite($file,"<?xml version='1.0' encoding='UTF-8'?>
<timesheet xmlns='http://www.w3.org/ns/SMIL'>

	<!-- slideshow -->
	<!-- indiquer la durée totale du diaporama juste ci-dessous : variable 'dur'. Si le fichier son ou videéo est plus long, il continue à se jouer. -->
	<excl timeAction='intrinsic' dur='".$dureePresentation."' mediaSync='#talk' controls='#timeController' navigation='arrows; hash'>

	<!-- Indiquer ci-dessous les instants d'apparition de chaque diapo par rapport au fichier son ou vidéo.
	On peut ajouter des diapos, attention de bien respecter la structure, notamment le # devant le nom de la diapo. Attention, c'est l'identité HTML, pas le nom du fichier image qu'on indique ici. Donc sous la forme #diapo0nn --> 

".$blocMarqueurs."   
	</excl>

	<!-- swap video and slideshow -->
	<par timeAction='class:swapped'>
		<item select='body' begin='swap.click' end='swap.click'/>
	</par>

</timesheet>");

	fclose($file);

?>