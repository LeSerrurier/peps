<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- CSS de Boostrap - CSS de la page -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="style.css" rel="stylesheet">
		<title> Correction </title>
	</head>
	
	<!-- Librairie de Jquery et de Boostrap -->
	<script src="bootstrap/js/jquery.min.js"></script>	
	<script src="bootstrap/js/bootstrap.min.js"></script>
	
	<script type="text/javascript" src='include/fonctions.js'></script>
	
	
	<script>
	
		//Quand le document est prêt
		$(document).ready(function() {				
		
			<?php
				//Si on a reçu en GET la variable 'data' (nomDossierUt), alors on prérempli les champs du formulaire
				if(isset($_GET['data'])){
					//Récupère le nom du dossier de l'utilisateur ainsi que le nom de son Exodus
					$nomDossierUt = $_GET['data'];	
					$nomExodus = $_GET['nom'];
					
			?>
			
			//Quand on clic sur l'id 'nombreDePartie'
			$(document).on('click','#nombreDePartie',function() {		

					//Puisque nous avons déjà les fichiers sur le serveur, pas besoin de les upload à chaque fois et donc, ils ne sont plus obligatoires
					$('#fichierMarqueur').removeAttr('required');
					$('#fichierPDF').removeAttr('required');
					$('#fichierAudio').removeAttr('required');
					$('#fichierImage').removeAttr('required');
					
					//Ajoute un input de type 'hidden' à l'id 'formulaire' pour conserver le nom du dossier de l'utilisateur quand il va envoyer le formulaire
					$('<input>').attr({
						type: 'hidden',
						name: 'nomDossierUt',
						value: <?php echo "'".$nomDossierUt."'\n";?>
					}).appendTo('#formulaire');
					
					
			<?php 
				//Inclut le code pour préremplir le formulaire - on ferme la balise de la fonction en cours - on ferme le if du code PHP plus haut
				include('preRemplirFormulaire.php');
				echo "			});\n";
				
				} 
			?>
			
			//Pour activer la fonctionne définie plus haut, on utilise la fonction 'trigger' qui va enclencher un clic sur l'id 'nombreDePartie' qui est un
			//input de type hidden, donc aucun risque que l'utilisateur clic sur ce champ
			$('#nombreDePartie').trigger('click');
			
			//Si il y a plusieurs Parties dans la TDM (donc que le nombre de class 'niveau1' soit supérieure à 1) on montre le bouton pour enlever la dernière partie
			if($(".niveau1").length > 1){
				$("#moinsPartie").show();
			}
			
			//Pour toutes les class 'selectDiapo', si le numéro de diapo est au dessus du nombre de diapo, on passe la valeur à 'diapo001'
			$('.selectDiapo').each(function (i, champ) {
				//Convertit la chaîne de caractère 'diapoXXX' à un chiffre en base de 10 'XXX'
				numeroDiapo = parseInt($(champ).val().substring(5,9),10);
				if( numeroDiapo > $("#nombreDeDiapo").val()){
					$(champ).val("diapo001");
				}
			});
			
			//Si on clique sur le bouton '+' de la section Marqueur, affiche la liste des marqueurs ou la cache en changeant par '+' et '-'
			$('#divPisteMarqueur').click(function(){
				if($('#listeMarqueur').is(":visible")){
					$('#listeMarqueur').hide();
					$('#divPisteMarqueur').text('+');
				}else{
					$('#listeMarqueur').show();
					$('#divPisteMarqueur').text('-');
				}
			});
	
		});
		
	</script>
	
	
	<body>	
		
		<!-- Formulaire en post qui permet d'envoyer des fichiers - Le lien où sont envoyés les données est ajoutée en jquery dans le fichier "include/fonctions.js" -->
		<form id='formulaire' method="post" enctype="multipart/form-data">
		
			<?php include('include/formulaire.html');?>
				
			<div id='sectionMarqueur'>
				
				<div class="row justify-content-md-center">
					<h1 class='center-block titreSection'> 
						<span> Marqueurs </span>
						<button type="button" id='divPisteMarqueur' class="btn btn-primary btn-lg pull-right"> + </button>
					</h1>
				</div>	

				<div style='display:none;' id='listeMarqueur'>
					<div id='ligneMarqueur1' class="ligneMarqueur form-group row">
						<label for="nomMarqueur 1" class="marqueur col-lg-2 col-form-label">Marqueur 1</label>
						<div class="inputMarqueur col-lg-2">
							<input id="nomMarqueur1" class='form-control' name="nomMarqueur1" type="time" step='1' />
						</div>	
					</div>
				</div>
			
			</div>
			
			<!-- Bouton 'valider' pour envoyer le formulaire -->
			<div style='height:50px;'></div>
			<div class="form-row justify-content-center"> 
				<button type="submit" id='valider' class="btn btn-success btn-lg col-lg-6"> Valider </button>
			</div> 
			
		</form>
			
	</body>
	
</html>