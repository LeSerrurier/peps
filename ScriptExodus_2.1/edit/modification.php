<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- CSS de Boostrap - CSS de la page -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="style.css" rel="stylesheet">
		<title> Modification </title>
	</head>
	
	<!-- Librairie de Jquery et de Boostrap -->
	<script src="bootstrap/js/jquery.min.js"></script>	
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<script type="text/javascript" src='include/fonctions.js'></script>


	<body>
				
		<!-- Formulaire en post qui permet d'envoyer des fichiers - Le lien où sont envoyés les données est ajoutée en jquery dans le fichier "include/fonctions.js"-->
		<form id='formulaire' method="post" enctype="multipart/form-data">
		
			<!-- Pour retourner à l'accueil -->
			<p> <a class='boutonMode btn btn-primary btn-block btn-lg' href='../../index.html' > Accueil </a> </p>	
			
			
			<div class="row justify-content-md-center">
				<h1 class='center-block titreSection'> Instructions </h1>
			</div> 
			
			<p> Seul le fichier ZIP de votre projet est obligatoire. </p>
			<p> Cependant, si vous voulez changer un des fichiers, comme le PDF par exemple, vous pouvez directement le faire ici.	</p>


			<div class="row justify-content-md-center">
				<h1 class='center-block titreSection'> Fichiers </h1>
			</div> 
			
			
			<div class="form-group row justify-content-center">
				
				<div class="col-lg-3">
					<input id='fichierZip' name="fichierZip" class='fichier custom-file-input' type="file" accept='.zip' required='required' />
					<label for="fichierZip" class="custom-file-label">Zip Exodus</label>
				</div>
				<div class="col-lg-4"></div>
				
			</div>	
			
			<div class="form-group row justify-content-center">

				<div id='delimitationModif' class="col-lg-7"></div>
				
			</div>
			
			<div class="form-group row justify-content-center">
				
				<div class="col-lg-3">
					<input id='fichierMarqueur' name="fichierMarqueur" class='fichier custom-file-input' type="file" accept='.txt'='required' />
					<label for="fichierMarqueur" class="custom-file-label">Marqueur</label>
				</div>
				<div class="col-lg-1"></div>
				<div class="col-lg-3">
					<input id='fichierPDF' name="fichierPDF" class="fichier custom-file-input" type="file" accept='.pdf' />
					<label for="fichierPDF" class="custom-file-label">PDF</label>
				</div>
				
			</div>
			
			<div class="form-group row justify-content-center">
				<div class="col-lg-3">
					<input id='fichierImage' name="fichierImage" class='fichier custom-file-input' type="file" accept='.png,.jpg,.jpeg' />
					<label for="fichierImage" class="custom-file-label">Photo</label>
				</div>
				<div class="col-lg-1"></div>
				<div class="col-lg-3">
					<input id='fichierAudio' name="fichierAudio" class='fichier custom-file-input' type="file" accept='.mp3,.ogg,.aac' />
					<label for="customFile" class="custom-file-label">Audio</label>
				</div>
			</div>
			

			<!-- Variable caché pour nous permettre d'effectuer certaines actions à la réception du formulaire -->
			<input id='modifData' name='modifData' type='hidden' value='vrai' />
			
			<!-- Bouton 'valider' pour envoyer le formulaire -->
			<div style='height:50px;'></div>
			<div class="form-row justify-content-center"> 
				<button type="submit" id='valider' class="btn btn-success btn-lg col-lg-6"> Valider </button>
			</div> 
			
		</form>

	</body>
	
</html>