<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- CSS de Boostrap - CSS de la page -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="style.css" rel="stylesheet">
		<title> Création </title>
	</head>

	<!-- Librairie de Jquery et de Boostrap -->
	<script src="bootstrap/js/jquery.min.js"></script>	
	<script src="bootstrap/js/bootstrap.min.js"></script>
		
	<script type="text/javascript" src='include/fonctions.js'></script>

	<body>    
   
		<form id='formulaire' method="post" enctype="multipart/form-data">
		
			<!-- Pour retourner à l'accueil -->
			<p> <a class='boutonMode btn btn-primary btn-block btn-lg' href='../../index.html' > Accueil </a> </p>	
		
			<?php include('include/formulaire.html'); ?>
			
			<!-- Bouton 'valider' pour envoyer le formulaire -->
			<div style='height:50px;'></div>
			<div class="form-row justify-content-center"> 
				<button type="submit" id='valider' class="btn btn-success btn-lg col-lg-6"> Valider </button>
			</div> 
		
		</form>

	</body>
	
</html>