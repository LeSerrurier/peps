		
		$(document).ready(function() {
				
			//Ajoute un paterne à tous les champs 'text' qui autorise les caractères suivant : les majuscules, minuscules, lettres accentués, tirets, nombres,slash, apostrophe et les cédilles
			$(":text").attr("pattern","[A-Z a-z ÂÊÎÔÛÄËÏÖÜÀÆÇÉÈŒÙÑ ñáàâäæœéèêëíîïóúüô -- _ / 0-9 çÇ ? : ']+");
			
			//Ajoute l'attribut 'action' aux balises 'form' avec comme valeur le lien relatif de la page de traitement
			$("form").attr("action","../traitementFormulaire.php");
			
			//Si il y a plusieurs parties dans la TDM (donc que le nombre de class 'niveau1' soit supérieure à 1) on montre le bouton qui supprime la dernière partie
			if($(".niveau1").length > 1){
				$("#moinsPartie").show();
			}
			
			//Si il y a une valeur dans le nombre de diapo, permet de remplir les options du select de la partie 1
			if( $("#nombreDeDiapo").val() != null){
				majNombreDiapo();
			}
				
			//Ajoute une partie quand on clique sur le bouton 'Ajouter une partie'
			//Montre le bouton 'Supprimer la dernière partie", met à jour le nombre de partie et ajoute les caractères autorisés
			$('#plusPartie').click(function(){
				ajouterUnePartie();
				nombrePartie = $(".niveau1").length;
				$("#nombreDePartie").val(nombrePartie);
				$("#moinsPartie").show();
				$("#nomPartie" + nombrePartie).attr("pattern","[A-Z a-z ÂÊÎÔÛÄËÏÖÜÀÆÇÉÈŒÙÑ ñáàâäæœéèêëíîïóúüô -- _ / 0-9 çÇ ? : ']+");
			});
			
			//Quand on clique sur le bouton 'Supprimer la dernière partie'
			$(document).on('click','#moinsPartie',function(){
				nombrePartie = $(".niveau1").length;
				//Supprime la dernière partie, donc la dernière id 'conteneurX' de la div 'maListe'
				$('#maListe #conteneur'+nombrePartie).remove();
				
				//Cache le bouton 'Supprimer la dernière partie' si le nombre de 'niveau1' est à 2 (donc va passer à 1 après le clic)
				if (nombrePartie == 2){
					$("#moinsPartie").hide();
				}
				//Met à jour le nombre de partie
				$("#nombreDePartie").val($(".niveau1").length-1);
			});
			
			//Ajoute une sous partie quand on clique sur le bouton '+'
			$(document).on('click', '#maListe .plusSousPartie',function(){
				//On récupère la valeur de l'id du 'conteneurX' du bouton '+' que l'on vient de cliquer
				idConteneur = $(this).parent().parent().parent().attr('id');
				//On appelle la fonction ajouterUneSousPartie avec idConteneur en paramètre pour lui dire que c'est à cette id qu'on veut ajouter une sous partie
				ajouterUneSousPartie(idConteneur);
				//Récupère le nombre X de partie et Y de sous partie pour obtenir l'id de 'nomPartieX_Y' afin de lui ajouter les caractères autorisés
				nombrePartie = $(".niveau1").length;
				nombreSousPartie = $("#conteneur" + nombrePartie + " .niveau2").length;
				$("#nomSousPartie" + nombrePartie + "_" + nombreSousPartie).attr("pattern","[A-Z a-z ÂÊÎÔÛÄËÏÖÜÀÆÇÉÈŒÙÑ ñáàâäæœéèêëíîïóúüô -- _ / 0-9 çÇ ? : ']+");
			});
			
			//Supprime la dernière sous partie de la partieX quand on clique sur le bouton '-'
			$(document).on('click','.moinsSousPartie',function(){
				//On récupère la valeur de l'id du 'conteneurX' du bouton '-' que l'on vient de cliquer
				idConteneur = $(this).parent().parent().parent().attr('id');
				//Supprime la dernière class 'niveau2' de l'id 'conteneurX'
				$("#" + idConteneur + ' .niveau2:last').remove();
			});
				
			//Quand le nombre de diapo change on appelle la fonction 'majNombreDiapo'
			$("#nombreDeDiapo").change(function(){
				//Si le nombre est inférieure à 0 ou est null, on ajuste la valeur à 1
				if($("#nombreDeDiapo").val() < 1 || $("#nombreDeDiapo").val() == null){
					$("#nombreDeDiapo").val("1");
				}
				majNombreDiapo();
			});
			
			//Si appuie sur la touche 'entrée' dans le champ 'Nombre de diapo', bloque l'envoie du formulaire et permet de 'valider' la valeur du champ
			$(document).on('keypress',' #nombreDeDiapo', function(e) {
				//Si la touche est 'entrée' alors
				if (e.which == 13) { 
					//L'action par défaut ne doit pas être prise en compte comme elle le serait normalement,
					//l'action par défaut étant d'envoyer le formulaire en appuyant sur la touche 'entrée'
					e.preventDefault();
					//Ensuite, pour que la valeur soit prit en compte par jquery, on change le focus sur le champ juste en-dessous puis
					$('#nomPartie1').focus();
					//On revient sur le champ de base
					$('#nombreDeDiapo').focus();
				}
			});
			
			//Ici même chose mais pour les inputs de type 'time'
			$(document).on('keypress',' input[type="time"]', function(e) {
				if (e.which == 13) { 
					e.preventDefault();
					//Contrairement au champ 'Nombre de diapo' de type 'number', pour que la valeur soit prit en compte, on peut remettre le focus sur eux même
					$(this).focus();
				}
			});
			
			//Affiche le nom du fichier dans le champ du fichier
			$('.custom-file-input').on('change', function() { 
			   let fileName = $(this).val().split('\\').pop(); 
			   $(this).next('.custom-file-label').addClass("selected").html(fileName); 
			});
			
			//Quand on appuie sur le bouton valider, ça cache le formulaire et ajoute le bloc du chargement qui est configuré dans la page CSS
			$('#valider').click(function(){
				//Il faut vérifier que l'utilisateur a remplit tous les champs
				probleme = false;
				//Pour tous les champs obligatoires, on regarde s'il n'y a pas de valeur, et si c'est le cas, on passe 'probleme' à 'vrai'
				$(':required').each(function (i, champ) {
					if( $(champ).val() == ''){
						probleme = true;
					}
				});
				//S'il n'y pas de problème, donc 'probleme' à 'faux', on cache le formulaire et on ajoute le temps de chargement
				if(probleme == false){
					divChargement = '	<div id="chargement" class="form-group row justify-content-center">' +
										'<div class="loader"></div>' +
									'</div>';
					$('#formulaire').hide();
					$('body').append(divChargement);
				}
			});
			
		});
			
		//Met à jour le nombre d'option dans les select des class 'selectDiapo'
		function majNombreDiapo() {
			//On récupère le nombre diapo en tout
			nombreDeDiapo = $("#nombreDeDiapo").val();
			//Comme le select de 'diapoPartie1' ne peut pas être supprimé, on se base sur sa valeur pour avoir le nombre de diapo avant la modification du nombre de diapo
			nombreDeDiapoExistante = $("#diapoPartie1 option").length+1;
			//Si le nombre de diapo à augmenté alors on ajoute des options, sinon on supprime des options
			if(nombreDeDiapo >= nombreDeDiapoExistante) {
				//Tant qu'on a pas atteint ce nouveau nombre de diapo on ajoute des options
				for(i = nombreDeDiapoExistante; i <= nombreDeDiapo; i++){
					nomDiapo = setNomDiapo(i);
					//Ajoute à toutes les class 'selectDiapo' l'option diapoXXX avec le masque correspondant
					$(".selectDiapo").append("<option value=" + nomDiapo + "> " + nomDiapo + " </option>");
				}
			} else {
				//Tant qu'on a pas atteint ce nouveau nombre de diapo on enlève des options
				for(i = nombreDeDiapoExistante; i > nombreDeDiapo; i--){
					nomDiapo = setNomDiapo(i);
					//Supprime l'option diapoXXX de toutes les class 'selectDiapo'
					$(".selectDiapo option[value='" + nomDiapo + "']").remove();
				}
			}

			//Si il y a bien une liste de marqueur alors
			if($("#listeMarqueur .ligneMarqueur").length != 0) {
				//Si le nombre de marqueur est plus grand que le nombre de diapo, on supprime jusqu'à atteindre le nombre de diapo
				//Sinon si le nombre de marqueur est plus petit que le nombre de diapo, on ajoute jusqu'à atteindre le nombre de diapo
				nombreDeMarqueur = $("#listeMarqueur .marqueur").length;

				if(nombreDeMarqueur > nombreDeDiapo){
					while(nombreDeMarqueur > nombreDeDiapo && nombreDeMarqueur > 1 ){
						supprimerDernierMarqueur();
						nombreDeMarqueur = $("#listeMarqueur .marqueur").length;
					}
				} else {
					while(nombreDeMarqueur < nombreDeDiapo){
						ajouterUnMarqueur("00:00:00");
						nombreDeMarqueur = $("#listeMarqueur .marqueur").length;
					}
				}
			}
		}
		
		//Remplit le select passé en argument
		function setNombreDiapo(paramDiapo){
			//On récupère le nombre de diapo en tout
			nombreDeDiapo = $("#nombreDeDiapo").val();
			//Tant qu'on a pas atteint ce nombre
			for(i = 1; i <= nombreDeDiapo; i++){
				nomDiapo = setNomDiapo(i);
				//On ajoute au select passé en argument, l'option diapoXXX avec le masque correspondant
				$(paramDiapo).append("<option value=" + nomDiapo + "> " + nomDiapo + " </option>");
			}
		}
		
		//Attribue le masque diapoXXX
		function setNomDiapo(i){
			//Si on est dans les 9 premières diapos on applique le masque diapo00X, sinon on applique le masque diapo0XX
			if( i <= 9 ){
				nomDiapo = "diapo00" + i;
			} else {
				nomDiapo = "diapo0"+ i;
			}
			return nomDiapo;
		}
		
		
		//Ajoute une partie à la TDM
		function ajouterUnePartie(){
			//o+On récupère le nombre de class 'niveau1' (donc nombre de partie)
			nombrePartie = $(".niveau1").length + 1;
			//Code html pour ajouter une partie - 'nombrePartie' qui permet d'adapter le nom des id et de l'affichage en fonction du nombre de partie
			listeDynamique =	"<div id='conteneur" + nombrePartie + "'>" +	
									"<div id='partie' class='niveau1 form-group row'>" + 
										"<label for='nomPartie" + nombrePartie + "' class='col-form-label col-lg-2'> Nom Chapitre " + nombrePartie + "</label>" +
										"<input id='nomPartie" + nombrePartie + "' class='col-lg-3' name='nomPartie" + nombrePartie + "' type='text' required />" + 
										"<div class='col-lg-4 row'>" +
											"<label for='diapoPartie" + nombrePartie + "' class='col-form-label col-lg-6'> Numéro diapo </label>" +
											"<SELECT id='diapoPartie" + nombrePartie + "' class='selectDiapo custom-select col-lg-6' name='diapoPartie" + nombrePartie + "' required></SELECT>" +
										"</div>" +
										"<div class='col-lg-3 justify-content-center row'>" +
											"<button class='plusSousPartie btn btn-primary col-lg-4' type='button'> + </button>" +
											"<div class='col-lg-1'></div>" +
											"<button class='moinsSousPartie btn btn-danger col-lg-4' type='button'> - </button>" +
										"</div>" +
									"</div>" +
								"</div>";
			//On ajoute à notre liste 'maListe' le code html 
			$("#maListe").append(listeDynamique);	
			//On appelle la fonction 'setNombreDiapo' avec en argument le select du dernier 'niveau1' qu'on vient de créer
			setNombreDiapo("#diapoPartie" + nombrePartie);				
		}
		
		//Ajoute une sous partie à l'id de la partieX
		function ajouterUneSousPartie(idPartie){
			//On récupère la position de la partie de la sous partie
			positionPartie = $("#" + idPartie).index() + 1;
			//On récupère le nombre d’éléments li dans l'id de la partie (donc nombre de sous partie de la patieX)
			nombreSousPartie = $("#" + idPartie + " .niveau2").length + 1;
			//Besoin de savoir dans quelle partie on se trouve + le nombre de sous partie pour avoir un id unique (diapoSousPartieX_Y)
			idUnique = "" + positionPartie + "_" + nombreSousPartie;
			//Code html pour ajouter une sous partie - 'nombreSousPartie' qui permet d'adapter le nom des id et de l'affichage en fonction du nombre de sous partie
			listeDynamique =	"<div id='sousPartie" + idUnique + "' class='niveau2 form-group row'>" +
									"<div class='col-lg-1'></div>" +
									"<label for='nomSousPartie" + idUnique + "' class='col-form-label col-lg-2'> Sous Chapitre " + nombreSousPartie + "</label>" +
									"<input id='nomSousPartie" + idUnique + "' class='col-lg-2' name='nomSousPartie" + idUnique + "' type='text' required />" + 
									"<div class='col-lg-1'></div>" +
									"<div class='col-lg-2 row'>" +
										"<SELECT id='diapoSousPartie" + idUnique + "' class='selectDiapo custom-select' name='diapoSousPartie" + idUnique + "' required ></SELECT>" +
									"</div>" +
								"</div>";
			//On ajoute à la liste 'partieX' le code html et on appelle la fonction 'setNombreDiapo'
			$("#" + idPartie).append(listeDynamique);
			//On appelle la fonction 'setNombreDiapo' avec en argument le select du dernier li qu'on vient de créer
			setNombreDiapo("#diapoSousPartie" + idUnique);
		}
		
		//Ajoute un élément à la liste des marqueurs avec comme argument le temps du marqueur
		function ajouterUnMarqueur(temps){
			//Récupère le nombre d’éléments +1 pour ne pas avoir le même nom que le dernier élément
			tailleListe = $("#listeMarqueur .marqueur").length+1;
			//Récupère le nombre de ligne de marqueur grâce à la class 'ligneMarqueur' présente dans toutes les div de chaque ligne
			ligneActuelle = $('.ligneMarqueur').length;
			//Code html pour un élément 'marqueur' 
			marqueurHtml = "	<label for='nomMarqueur" + tailleListe + "' class='marqueur col-lg-2 col-form-label'>Marqueur " + tailleListe + " </label>" +
								"<div class='inputMarqueur col-lg-2'>" +
									"<input id='nomMarqueur" + tailleListe + "' class='form-control' name='nomMarqueur" + tailleListe + "' value='" + temps + "' type='time' step='1' />" +
								"</div>";
			//S'il y a déjà 3 marqueurs sur la ligne, on ajoute une ligne et on ajoute un marqueur à cette ligne
			if($("#ligneMarqueur" + ligneActuelle + " .marqueur").length == 3){
				//Pour passer à la ligne suivante, il faut la position actuelle +1
				ligneSuivante = ligneActuelle +1;
				$('#listeMarqueur').append('<div id="ligneMarqueur' + ligneSuivante + '" class="ligneMarqueur form-group row"></div>');
				//Ajoute l’élément à la ligne suivante
				$('#ligneMarqueur' + ligneSuivante).append(marqueurHtml);
			} else {
				//Ajoute l’élément à la ligne actuelle
				$('#ligneMarqueur' + ligneActuelle).append(marqueurHtml);
			}
		}
		
		//Fonction qui permet de lire plus facilement le code plutôt que de répéter plusieurs fois la ligne jquery
		function modifierMarqueur(id, temps){
			$("#nomMarqueur" + id).val(temps);
		}
		
		//Supprime le dernier marqueur
		function supprimerDernierMarqueur(){
			//Récupère l'id de la ligne où il y a le dernier marqueur
			laLigneMarqueur = $("#listeMarqueur .marqueur").last().parent().attr('id');
			//Si c'est le dernier de la liste, on supprime la ligne
			if (($("#" + laLigneMarqueur + " .marqueur").length) == 1) {
				$("#listeMarqueur .ligneMarqueur").last().remove();
			//Sinon, on supprime le dernier libellé et input de la liste des marqueurs
			} else {
				$("#listeMarqueur .marqueur").last().remove();
				$("#listeMarqueur .inputMarqueur").last().remove();
			}
		}
		