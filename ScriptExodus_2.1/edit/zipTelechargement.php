<?php
	
	/*--------------------------------------------------------------------------------------*\
	
		Zip le dossier de l'utilisateur et le propose en téléchargement grâce au header
	
	\*--------------------------------------------------------------------------------------*/
	
	
	if(isset($_GET['data'])){
		
		//Récupère le nom du dossier de l'utilisateur, le nom de son Exodus et le nom de l'archive en lui rajoutant l'extension '.zip'
		$nomDossierUt = $_GET['data'];
		$nomExodus = $_GET['nom'];
		$nomArchive = $_GET['nomArchive'].'.zip';


		//Premier itérateur récursive pour parcourir le dossier - 
		//Deuxième itérateur récursive pour itérer sur le premier itérateur et donc permet de parcourir les sous dossiers à l’infini
		$dos_iterateur = new RecursiveDirectoryIterator("../dossierUt/".$nomDossierUt."/".$nomExodus."/");
		$iterateur = new RecursiveIteratorIterator($dos_iterateur);
		//Initialise la classe Archive à $zip - Crée une nouvelle archive 'Exodus_TitreCourt_Année-Mois-Jour
		$zip = new ZipArchive();
		$zip->open("../dossierUt/".$nomDossierUt."/".$nomArchive, ZipArchive::CREATE);
		//Pour chaque objet ($fichier) on fait 
		foreach ($iterateur as $fichier) {
			//Remplace dans le nom du chemin les '\' par des '/' - Récupère le dernier élément du chemin
			$fichier = str_replace('\\','/',$fichier);
			$verif = (substr(strrchr(($fichier),'/'),1));
			//Si le dernier élément n'est pas un '.' ou '..' alors
			if ($verif != '.' and $verif != '..'){
				//Récupère le chemin à partir du '$nomExodus/' ex: '../dossierUt/XXXX/Masque_Exodus/audio.html' => Masque_Exodus/audio.html
				//Puis enlève le 'nomExodus' pour ne pas Zipper le dossier mais son contenant
				$leNom = stristr($fichier, $nomExodus.'/');	
				$leNom = str_replace($nomExodus.'/','',$leNom);
				//Si ce n'est pas un dossier, on ajoute le fichier à l'archive en donnant le chemin relatif du fichier et son chemin absolue dans l'archive
				if (!(is_dir($fichier))){
					$zip->addFile($fichier,$leNom);
				}
			}
		}
		
		//Ferme l'archive et sauvegarde les modifications
		$zip->close();	
		
		header('Content-type: application/zip'); //Indique que c'est une archive
		header('Content-Transfer-Encoding: fichier'); //Transfert en binaire (fichier)
		header('Content-Disposition: attachment; filename="../dossierUt/'.$nomDossierUt.'/'.$nomArchive.'"'); //Nom de l'archive
		header('Content-Length: '.filesize('../dossierUt/'.$nomDossierUt.'/'.$nomArchive)); //Taille de l'archive
		header('Pragma: no-cache'); 
		header('Expires: 0');
		header("location:../dossierUt/".$nomDossierUt."/".$nomArchive); //Redirection vers le téléchargement de l'archive
	
	}
	
?>