<!DOCTYPE html>
<html lang="fr">

	<head>
		<meta charset="utf-8">
		<link href="edit/style.css" rel="stylesheet">
		<title>Traitement</title>
	</head>
    			
	<body>		
	
		<?php
		
			/*--------------------------------------------------------------------------------------*\
			
				C'est la page principale du traitement de l'application. Elle traite le formulaire de 
			la page 'creation.php' , 'modification.php' et 'correction.php'. Dans un 1er temps, on
			prépare l'espace de travail de l'utilisateur en créant un dossier unique et en nommant son 
			dossier Exodus, et on upload ses fichiers sur le serveur. Puis on fait une vérification, 
			selon le mode dans lequel se trouve l'utilisateur, qui permet de déterminer si l'upload 
			c'est bien passé, si l’envoie des données c'est bien passé et si l'utilisateur n'essayes 
			pas d’accéder à la page d'une manière détournée, on continue le traitement ou on envoie un
			message d'erreur et un bouton pour retourner à la page précédente.
				Ensuite, selon le mode, on va effectuer un certain traitement, copier le dossier 
			'Masque_Exodus', copier l'image et le son de l'utilisateur dans ce répertoire, lire le 
			fichier 'dataExodus'... Par contre ce qui est commun à tous les modes est l'écriture des 
			fichiers 'audio.html','previsualisation.html' et 'timesheet.smil'. Ainsi que le 'nettoyage'
			du répertoire, effectivement, peu importe le mode, on supprime les fichiers une fois 
			qu'ils ont été copiés. Et finalement on redirige l'utilisateur vers la page 'modeEdtion.html' 
			qui permet de prévisualiser son Exodus.
			
			\*--------------------------------------------------------------------------------------*/
	
	
		
			////-------------------------------- Fonctions --------------------------------\\\\
		
		
			include('fonctions.php');
			
			
			////--------------------------- Upload des fichiers sur le serveur ---------------------------------\\\\
			
			
			/* --------------------- Informations sur le tableau stockInfoFichierServeur: --------------------- *\
			||																									||
			||		Ce tableau associatif est très important et très pratique dans le programme, effectivement 	||
			||	il permet de connaître diverses informations intéressantes sur tous les fichiers, ce qui nous	||
			||	permet de faire des conditions facilement, de retrouver les informations facilement, d'améliorer|| 
			||	la lisibilité du code et d'avoir un traitement générique, on peut aisément ajouter ou enlever un||
			||	fichier juste en modifiant ce tableau. On y stock les informations suivantes :					||
			||																									||
			||			- chemin : ce qui correspond au chemin du fichier où il est stocké						||
			||			- extension : qui correspond à l'extension du fichier									||
			||			- nom : qui correspond au nom du fichier sans l'extension								||
			||			- upload :	qui est un boolean et permet de savoir si l'utilisateur a upload le fichier ||
			||						ou pas																		||
			||																									||
			||	Par exemple un fichier son 'conference.mp3' upload par l'utilisateur donne :					||
			||																									||
			||			array(	'chemin' => 'dossierUt/20192305093312/conference.mp3,							||
			||					'extension' => 'mp3',															||
			||					'nom' => 'conference',															||
			||					'upload' => TRUE )																||
			||																									||
			||		Pour débugger, il est conseillé d'afficher ce tableau dans les balises '<pre></pre>' et en 	||
			||	faisant un 'print_r($stockInfoFichierServeur)'													||
			\*--------------------------------------------------------------------------------------------------*/
			
			//Nomme le dossier Exodus de l'utilisateur sous le format : Masque_Exodus
			$nomExodus = "Masque_Exodus";
			
			//Nom des balises 'name' des fichiers dans le formulaire
			$tabNomFichierGenerique = array('fichierMarqueur','fichierPDF','fichierAudio','fichierImage','fichierData','fichierZip');
			$extensionsAutorisees = array(	'fichierMarqueur' => array('txt'),
											'fichierData' => array('txt'),
											'fichierPDF' => array('pdf'),
											'fichierZip' => array('zip'),
											'fichierAudio' => array('mp3','ogg','aac'),
											'fichierImage' => array('png','jpg','jpeg'));
			
			//Initialisation des champs pour tous les fichiers du tableau 'stockInfoFichierServeur'
			foreach($tabNomFichierGenerique as $nomFichier){
				$stockInfoFichierServeur[$nomFichier] = array('chemin'=> '', 'extension'=> '', 'nom'=>'', 'upload'=> FALSE);
			}

			//Si on vient du mode Correction avec donc le numéro du dossier en POST dans une balise hidden on stock les informations sur l'image et l'audio
			//qui sont par défaut dans son Exodus, pour pouvoir écrire le fichier 'audio.html' sans soucis si l'Utilisateur n'upload pas de nouveau fichier
			//S'il upload des nouveaux fichiers, les nouvelles informations écraseront les anciennes dans la suite du programme
			if (isset($_POST['nomDossierUt'])){
				$nomDossierUt = $_POST['nomDossierUt'];

				//Grâce à cette fonction, on récupère les informations du son du dossier Exodus et on stock ces informations dans le tableau 'stockInfoFichierServeur'
				$fichierDefaut = trouverFichier('fichierAudio');
				$stockInfoFichierServeur['fichierAudio'] = array('chemin'=> $fichierDefaut['chemin'], 'extension'=> $fichierDefaut['ext'], 'nom'=> $fichierDefaut['nom'], 'upload'=> FALSE);

				//Grâce à cette fonction, on récupère les informations de l'image du dossier Exodus et on stock ces informations dans le tableau 'stockInfoFichierServeur'
				$fichierDefaut = trouverFichier('fichierImage');
				$stockInfoFichierServeur['fichierImage'] = array('chemin'=> $fichierDefaut['chemin'], 'extension'=> $fichierDefaut['ext'], 'nom'=> $fichierDefaut['nom'], 'upload'=> FALSE);

			//Sinon cela veut dire qu'on est en mode Création ou Modification et qu'il faut donc créer un répertoire pour l'utilisateur	
			} else {
				//Crée un dossier dans 'dossierUt/' avec comme nom la date et l'heure sous le format : AAAAMMJJHHMMSS
				$nomDossierUt = date("YmdHis");
				mkdir('dossierUt/'.$nomDossierUt);
			}
			
			//Variable qui permet d'être sûr que l'Utilisateur, lorsqu'il est en mode Création, fournit bien 4 fichiers
			$nombreFichierRecu = 0;
			//Parcourt tout le tableau 'tabNomFichierGenerique' - Si le fichier a été réceptionné et qu'il n'y pas d'erreur, alors..
			for($i = 0; $i < sizeof($tabNomFichierGenerique); $i++){
				if( isset($_FILES[$tabNomFichierGenerique[$i]]) and $_FILES[$tabNomFichierGenerique[$i]]['error'] == 0){
					//..on appelle la fonction 'traitementUploadFichier' pour upload les fichiers sur le serveur
					traitementUploadFichier($tabNomFichierGenerique[$i]);
					$nombreFichierRecu ++;	
				}
			}

			
			
			////-------------------------------- Traitement du formulaire --------------------------------\\\\

			
			//Si on est en mode Création, vérification de la bonne réception des POST en testant la bonne déclaration d'un champ obligatoire du formulaire et
			//qu'on a bien reçu les 4 fichiers
			//OU 
			//Si on est en mode Modification, vérification de la bonne réception de la variable 'modifData' qui nous indique qu'on est en mode Modification
			//OU
			//Si on est en mode Correction, vérification de la bonne réception de la variable 'nomDossierUt' qui nous indique qu'on a déjà crée un dossier Utilisateur
			//et donc qu'on est en mode Correction
			if ((isset($_POST['titreConference']) && $nombreFichierRecu == 4) or (isset($_POST['modifData'])) or (isset($_POST['nomDossierUt']))){
	
				//Si on est pas en mode Modification, on récupère les données du formulaire
				if (!(isset($_POST['modifData']))){
					
					//Récupération des données du formulaire
					$titreConference = $_POST['titreConference'];
					$dateConference = $_POST['dateConference'];
					$nomEnseignant = $_POST['nomEnseignant'];
					$prenomEnseignant = $_POST['prenomEnseignant'];
					$service = $_POST['service'];
					$employeur = $_POST['employeur'];
					$dureePresentation = $_POST['dureePresentation'];
					$nombreDeDiapo = $_POST['nombreDeDiapo'];
					$nombreDePartie = $_POST['nombreDePartie'];
					
					if (isset($_POST['priseSon'])){ 
						$priseSon = $_POST['priseSon'];
					} else {
						$priseSon = "";
					}
					if (isset($_POST['realisation'])){ 
						$realisation = $_POST['realisation'];
					} else {
						$realisation = "";
					}
					
					//Met sous le format : Prénom NOM
					$prenomEnseignant = ucfirst(strtolower($prenomEnseignant));
					$nomEnseignant = strtoupper($nomEnseignant);

				}
								
				//Si on est en mode Création (donc pas Correction ni Modification donc qu'on a pas reçu 'nomDossierUt' ou 'modifData') ...
				if ((!(isset($_POST['nomDossierUt']))) and (!(isset($_POST['modifData'])))){
					//...copie le dossier du Masque_Exodus générique, dans le dossier de l'utilisateur - On a donc besoin du nom de son dossier ainsi que le nom de son Exodus
					copierDossier('Masque_Exodus/', 'dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/');
				}
				
				//Si on est en mode Modification, on inclut le programme 'traitementFormulaireModif.php' qui permet de traiter le formulaire du mode Modification
				if ((isset($_POST['modifData']))){
					include('include/traitementFormulaireModif.php');
				}
				
				//Si on a reçu le fichier image, donc que l'attribut 'upload' est à TRUE, on déplace l'image dans le dossier de l'Exodus
				if ($stockInfoFichierServeur['fichierImage']['upload'] == TRUE){
				
					//Comme il est possible d'upload des images avec différentes extensions, il faut supprimer à chaque fois l'ancienne image car si l'extension change, 
					//le fait de copier une image au même nom mais avec une extension différente, n’écrasera pas l'image déjà présente
					//Analyse le dossier images de l'Exodus de l'Utilisateur
					$dossier = scandir('dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/images');
					//Pour chaque fichier on scinde son nom et son extension pour regrouper ces informations dans un tableau ([0] = nomFichier, [1] = extension fichier)
					foreach ($dossier as $fichier){
						$decoupeFichier = explode('.', $fichier);						
						//Si ce n'est pas REVOIR , comme le dossier contient qu'un seul fichier, qu'on ne connaît pas son nom, on cherche un fichier qui a une extension acceptable
						if($fichier != '.' and $fichier != '..' and $decoupeFichier[0] != 'logo' and ($decoupeFichier[1] == 'jpg' or $decoupeFichier[1] == 'png' or $decoupeFichier[1] == 'jpeg')){
							//Supprime le fichier image existant...
							unlink('dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/images/'.$fichier);
						}
					}

					//... puis copie la photo qu'a upload l'Utilisateur et la supprime de son 'dossierUt'
					$nomDest = $stockInfoFichierServeur['fichierImage']['nom'].'.'.$stockInfoFichierServeur['fichierImage']['extension'];
					copy($stockInfoFichierServeur['fichierImage']['chemin'], 'dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/images/'.$nomDest);
					unlink('dossierUt/'.$nomDossierUt.'/'.$nomDest);
				}
				
				//Si on a reçu le fichier audio, donc que l'attribut 'upload' est à TRUE, on déplace le fichier son dans le dossier de l'Exodus
				if ($stockInfoFichierServeur['fichierAudio']['upload'] == TRUE){		
				
					//Comme il est possible d'upload des fichiers audio avec différentes extensions, il faut supprimer à chaque fois l'ancien fichier audio car si l'extension change, 
					//le fait de copier un fichier audio au même nom mais avec une extension différente, n'écrasera pas le fichier audio déjà présent
					//Analyse le dossier 'sons' de l'Exodus de l'Utilisateur
					$dossier = scandir('dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/sons');
					//Pour chaque fichier on scinde son nom et son extension pour regrouper ces informations dans un tableau ([0] = nomFichier, [1] = extension fichier)
					foreach ($dossier as $fichier){
						$decoupeFichier = explode('.', $fichier);
						//Si ce n'est pas REVOIR , comme le dossier contient qu'un seul fichier, qu'on ne connaît pas son nom, on cherche un fichier qui a une extension acceptable
						if($fichier != '.' and $fichier != '..' and ($decoupeFichier[1] == 'mp3' or $decoupeFichier[1] == 'ogg' or $decoupeFichier[1] == 'aac')){
							//Supprime le fichier son existant
							unlink('dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/sons/'.$fichier);
						}
					}
					//... puis copie l'audio qu'a upload l'Utilisateur dans son dossier Exodus et la supprime de son 'dossierUt'
					$nomDest = $stockInfoFichierServeur['fichierAudio']['nom'].'.'.$stockInfoFichierServeur['fichierAudio']['extension'];
					copy($stockInfoFichierServeur['fichierAudio']['chemin'],'dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/sons/'.$nomDest);
					unlink('dossierUt/'.$nomDossierUt.'/'.$nomDest);
				}
				
				
				
				/*---------------------------------------------Information sur la manipulation des fichiers----------------------------------------------*\
				||fopen : ouvre un fichier, prend en argument le fichier et un mode d'ouverture, ci-dessous ceux utilisés								||
				||fwrite : écrit dans un fichier, prend en argument le chemin du fichier et la chaîne de caractères à écrire							||
				||fclose : ferme un fichier, prend en argument le chemin du fichier																		||									
				||'r' : Ouvre en lecture seule, et place le pointeur au début du fichier																||
				||'w' : Ouvre un fichier en écriture seule, place le pointeur en début de fichier. Si le fichier existe déjà, son contenu est écrasé, 	||
				||		dans le cas contraire il crée le fichier																						||
				||'a+' : Ouvre en lecture et écriture ; place le pointeur à la fin du fichier. Si le fichier n'existe pas, on tente de le créer			||
				||file_put_contents() : 2 arguments, un pour le nom du fichier, un autre pour le contenu qu'on veut insérer								||
				||PHP_EOL : place automatiquement une fin de ligne compatible avec l’OS qui héberge le serveur											||
				||																																		||
				||	Dans les lignes HTML qu'on construit, il y a souvent des ' '; ils indiquent une ou plusieurs tabulations, cela permet de construire	||
				||un code bien indenté en sortie																										||
				\*--------------------------------------------------------------------------------------------------------------------------------------*/
				
				
				
				//Si on est en mode Correction, donc que la page 'previsualisation.html' a envoyé en POST le dossier de l'utilisateur,
				//et si l'utilisateur n'a pas upload un fichier marqueur, donc qu'on a pas rempli le tableau, on écrit le fichier des marqueurs
				//pour prendre en considération les potentielles modifications des marqueurs par l'utilisateur faites dans la section 'Marqueurs'
				if (isset($_POST['nomDossierUt']) and ($stockInfoFichierServeur['fichierMarqueur']['upload'] == FALSE)){
					include("include/ecritureMarqueurTxt.php");
				}

				//Si on a reçu le fichier, donc que l'attribut 'upload' est à TRUE, on convertit le PDF en images dans le dossier de l'Exodus
				if ($stockInfoFichierServeur['fichierPDF']['upload'] == TRUE){
					include("include/traitementPDF.php");
				}

				//Inclut les programmes pour : écrire les fichiers audio.html, timesheet.smil et dataExodus.txt
				include("include/ecritureAudioEtPrevisualisation.php");
				include("include/ecritureTimeSheetSmil.php");
				include("include/ecritureDataExodusTxt.php");

	
				//Une fois qu'on a finit de se servir du marqueur, on peut le supprimer du 'dossierUt'
				unlink('dossierUt/'.$nomDossierUt.'/'.$stockInfoFichierServeur['fichierMarqueur']['nom'].'.'.$stockInfoFichierServeur['fichierMarqueur']['extension']);
				
				//Redirige l'utilisateur vers son Exodus
				echo '<meta http-equiv="refresh" content="0;URL=dossierUt/'.$nomDossierUt.'/'.$nomExodus.'/edit/previsualisation.html">';
			
			//Sinon si il y a eu un problème dans la récéption des données, ou une autre anomalie on affiche un message d'erreur et on supprime le répertoire de l'utilisateur
			} else {
				
				//Message d'erreur avec un bouton pour retourner à la page précédente
				echo "	<p> Une erreur est survenu dans la réception des données. </p>
						<a class='bouton' href='javascript:history.go(-1)'>Retour</a>";
				
				
				//Si on a reçu au moins un fichier mais qu'il y a eu un problème au niveau du formulaire ou autre anomalie, qui survient dans les 10s, on supprime le dossier
				if( sizeof($stockInfoFichierServeur) != 0 && (date("YmdHis") - $nomDossierUt) < 10){
					supprimerDossier("dossierUt/".$nomDossierUt);
				}
			}

		?>
	
	</body>
	
</html>